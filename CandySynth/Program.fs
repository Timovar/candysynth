﻿namespace CandySynth

module Main =
  open NAudio.Wave
  
  let SampleRate = 44100
  let waveFormat = NAudio.Wave.WaveFormat(SampleRate, 1)
  
  let writeWavFile (filename : string) (samples : float array) = 
    use waveWriter = new WaveFileWriter(filename, waveFormat);
    waveWriter.WriteSamples(Array.map float32 samples, 0, samples.Length) // I guess we could just do everything in float32
    
  [<EntryPoint>]
  let main argv = 
    printfn "Making a wav file"

    let lfoSig = Signal.sine 1.0 1.0 0.0
    let modSig =  Signal.phaseModulatedWave Signal.sine lfoSig 1.0 450.0 0.0
               |> Signal.modulateAmplitude (Signal.linearAdsr 0.25 0.5 0.25 2.0 0.6) // imitation FM via PM, I guess
    let signal = Signal.phaseModulatedWave Signal.sine modSig 1.0 700.0 0.0
               |> Signal.modulateAmplitude (Signal.linearAdsr 0.1 0.9 0.25 2.0 0.6)
    let samples = Signal.generateSignal SampleRate (SampleRate * 2) signal
    writeWavFile "wavy.wav" samples

    let samples = (Signal.sine 1.0 300.0 0.0 |>                                          // Make a signal
                   Signal.modulateAmplitude (Signal.linearAdsr 0.25 0.5 0.25 2.0 0.6) |> // |
                   Signal.phaseModulatedWave Signal.triangle) 1.0 700.0 0.0 |>           // Modulate a tri wave with it
                   Signal.generateSignal SampleRate (SampleRate * 2)                     // Generate the samples
    writeWavFile "envelopedtripm.wav" samples


    let samples = Signal.sineMod (Signal.linearAdsr 0.25 0.5 0.25 2.0 0.6)
                                 (Signal.linear 200.0 400.0 2.0)
                                 (Signal.sine 1.0 300.0 0.0) |>
                  Signal.generateSignal SampleRate (SampleRate * 2)
    writeWavFile "spooky.wav" samples


    (* Make a zoop sound - a PM sound with increasing pitch - 
     * by increasing the frequency of the carrier and the PM signal at the same rate *)
    let samples = Signal.sineMod (Signal.linearAdsr 0.25 0.5 0.25 2.0 0.6)
                                 (Signal.linear 200.0 400.0 2.0)
                                 (Signal.sineMod (Signal.constant 1.0) 
                                                 (Signal.linear 300.0 600.0 2.0) 
                                                 (Signal.constant 0.0)) |>
                  Signal.generateSignal SampleRate (SampleRate * 2)
    writeWavFile "zoop.wav" samples
            

    (* The weird thing *)
    let samples = Signal.triangleMod (Signal.constant 1.0)
                                 (Signal.addSignals (Signal.constant 420.0) (Signal.sine 20.0 1.0 0.0))
                                 (Signal.constant 0.0) |>
                  Signal.generateSignal SampleRate (SampleRate * 100)
    writeWavFile "notvibrato.wav" samples     
    printfn "OK"


    (* The weird thing mystery deepens - this one "diverges" much slower *)
    let si t = if (t % 2.0) >= 1.0 then -t else t
    let samples = Signal.triangleMod (Signal.constant 1.0)
                                 (Signal.addSignals (Signal.constant 420.0) si)
                                 (Signal.constant 0.0) |>
                  Signal.generateSignal SampleRate (SampleRate * 100)
    writeWavFile "notvibrato2.wav" samples     
    printfn "OK"
    0
