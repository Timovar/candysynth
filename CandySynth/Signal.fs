namespace CandySynth
module Signal =
  type Sample = float
  type Signal = Sample array
  (* given a time (seconds), we can generate a sample at that time *)
  type SignalGenerator = float -> Sample
  (*given a an amplitude, frequency, and phase, we can generate a wave signal *)
  type WaveGenerator = float -> float -> float -> SignalGenerator
  
  (* Wave generators *)
  (* generate a sine wave *)
  let sine amplitude frequency phase t = amplitude * sin (2.0 * System.Math.PI * frequency * t + phase)
  (* generate a square wave *)
  let square (amplitude:float) frequency phase t = if (t * frequency + phase) % 1.0 > 0.5 then amplitude else -amplitude
  (* generate a triangle wave *)
  let triangle amplitude frequency phase t =
    match (t * frequency + phase) % 1.0 with
      | x when x < 0.25 -> 4.0 * x * amplitude
      | x when x >= 0.75 -> 4.0 * (x - 1.0) * amplitude
      | x -> 4.0 * (0.5 - x) * amplitude
  (* generate a sawtooth wave that goes linearly up and straight down *)
  let sawUp amplitude frequency phase t = (((t * frequency + phase) % 1.0) * 2.0 - 1.0) * amplitude
  (* generate a sawtooth wave that goes linearly down and straight up *)
  let sawDown amplitude frequency phase t = (((t * frequency + phase) % 1.0) * -2.0 + 1.0) * amplitude


  (* Modulated(?) wave generators. I decided that maybe these were the way to go after all. *)
  let sineMod ampSig freqSig phzSig t = (ampSig t) * sin (2.0 * System.Math.PI * (freqSig t * t + phzSig t))

  let triangleMod ampSig freqSig phzSig t =
    match (t * freqSig t + phzSig t) % 1.0 with
      | x when x < 0.25 -> 4.0 * x * ampSig t
      | x when x >= 0.75 -> 4.0 * (x - 1.0) * ampSig t
      | x -> 4.0 * (0.5 - x) * ampSig t

  let squareMod (ampSig : SignalGenerator) freqSig phzSig t = 
    if (t * freqSig t + phzSig t) % 1.0 > 0.5 
    then ampSig t 
    else -(ampSig t)
 
  let sawUpMod ampSig freqSig phzSig t = (((t * freqSig t + phzSig t) % 1.0) * 2.0 - 1.0) * ampSig t

  let sawDownMod ampSig freqSig phzSig t = (((t * freqSig t + phzSig t) % 1.0) * -2.0 + 1.0) * ampSig t

  (* Useful signal generators *)
  (* generate a constant signal of x. *)
  let constant x t = x
  (* generate a linear signal, from x0 at t=0 to x1 at t=length *)
  let linear x0 x1 length t = (x0 * (length - t) / length) + (x1 * t / length)
  
  (* generate a linear ADSR envelope *)
  let linearAdsr atkLength decLength relLength totalLength susLevel =
    let susLength = totalLength - (atkLength + decLength + relLength)
    let tDec = atkLength
    let tSus = tDec + decLength
    let tRel = tSus +  susLength
    let attack = linear 0.0 1.0 atkLength
    let decay = linear 1.0 susLevel decLength
    let sustain = constant susLevel
    let release = linear susLevel 0.0 relLength
    fun t ->
      match t with
        | t when t < tDec              -> attack t
        | t when t >= tDec && t < tSus -> decay (t - tDec)
        | t when t >= tSus && t < tRel -> sustain (t - tSus) 
        | _ (* t >= tRel *)            -> release (t - tRel)
      
  (* Signal modifiers *)
  
  (* generate a phase modulated wave *)
  let phaseModulatedWave (carrier : WaveGenerator) (signal : SignalGenerator) amplitude frequency phase t = 
    carrier amplitude 1.0 phase (frequency * t + signal t)
  
  (* modulate the amplitude of a wave *)
  let modulateAmplitude (carrier : SignalGenerator) (signal : SignalGenerator) t = carrier t * signal t
  
  (* generate the signal that is the sum of s1 and s2 *)
  let addSignals (s1 : SignalGenerator) (s2 : SignalGenerator) t = (s1 t) + (s2 t)
  
  (* Render a signalGenerator into a signal *)
  let generateSignal sampleRate samples signal = Array.init samples (fun i -> signal (float i / float sampleRate))
